﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace FamousTSDefinitionBuilder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        StringBuilder outputBuilder;

        private void BuildButton_Click(object sender, EventArgs e)
        {
            if (DirectoryBox.Text != "")
            {
                DirectoryInfo di = new DirectoryInfo(DirectoryBox.Text);
                if (di.Exists)
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "Typescript Definition File|*.d.ts";
                    sfd.FileName = "Famous.d.ts";
                    if (sfd.ShowDialog()== System.Windows.Forms.DialogResult.OK)
                    {
                        outputBuilder = new StringBuilder();
                        ParseDirectory(di);

                        using (StreamWriter sw=new StreamWriter(sfd.FileName))
                        {
                            sw.Write(outputBuilder.ToString());
                        }
                    }
                }
            }
        }

        private void DirectoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog()==DialogResult.OK)
            {
                DirectoryBox.Text = fbd.SelectedPath;
            }
        }
        void ParseDirectory(DirectoryInfo info)
        {
            foreach (DirectoryInfo  subDir in info.GetDirectories())
            {
                ParseDirectory(subDir);
            }
            foreach (FileInfo file in info.GetFiles("*.html"))
            {
                parseFile(file);
            }
        }
        void parseFile(FileInfo fileInfo)
        {
            string moduleName=fileInfo.Name.Substring(0,fileInfo.Name.IndexOf("."));

            StringBuilder builder = new StringBuilder();
            using (StreamReader sr=new StreamReader(fileInfo.FullName))
            {
                string body=WebUtility.HtmlDecode(sr.ReadToEnd());

                int functionStart = body.IndexOf("<div class=\"col1 method-block spacer-bar miniSpacer\">");
                while (functionStart < body.Length&&functionStart!=-1)
                {
                    int nextFunctionStart = body.IndexOf("<div class=\"col1 method-block spacer-bar miniSpacer\">", functionStart + 3);
                    if (nextFunctionStart == -1)
                    {
                        nextFunctionStart = body.Length;
                    }

                    string methodText = body.Substring(functionStart, nextFunctionStart - functionStart);
                    builder.Append(parseMethod(methodText,moduleName));

                    functionStart = nextFunctionStart;
                }
            }
            string builderRes = builder.ToString();
            if (builderRes!="")
            {
                outputBuilder.AppendFormat("interface {0}\n{{\n{1}}}\n", moduleName, builder);
            }
        }
        string parseMethod(string body,string fileName)
        {
            StringBuilder builder = new StringBuilder();
            int functionStartIndex = body.IndexOf("h3 id=\"") + 7;
                int endIndex = body.IndexOf("\"", functionStartIndex);
                string methodName = body.Substring(functionStartIndex, endIndex - functionStartIndex);
                if (methodName.IndexOf(fileName)==-1)
                {
                    builder.AppendFormat("\t{0}(",methodName);
                    int paramStartIndex = body.IndexOf("Parameters</h5>", functionStartIndex);
                    int returnStartIndex = body.IndexOf("Returns</h5>", functionStartIndex);


                    if (paramStartIndex > -1)
                    {
                        int paramsEndIndex = returnStartIndex > -1 ? returnStartIndex : body.Length;
                        paramStartIndex = body.IndexOf("<div class=\"col1-3 red miniSpacer\">", paramStartIndex) + 35;
                        while (paramStartIndex > 34)
                        {
                            int nameEndIndex = body.IndexOf("</div>", paramStartIndex);
                            string paramName = body.Substring(paramStartIndex, nameEndIndex - paramStartIndex);
                            int typeStartindex = body.IndexOf("<div class=\"col1-3 monospace\">",nameEndIndex)+30;
                            int typeEndIndex = body.IndexOf("</div>",typeStartindex);
                            string type = body.Substring(typeStartindex,typeEndIndex-typeStartindex);
                            if (type.IndexOf("Function")>-1)
                            {
                                type = "any";
                            }
                            else
                            {
                                if (type.IndexOf("Array.")>-1)
                                {
                                    string[] splitType = type.Split('.');
                                    type = splitType[1] + "[]";
                                }
                            }
                            type = type.Replace("=","").Replace("...","");
                            builder.AppendFormat("{0}:{1},",paramName,type);

                            paramStartIndex = body.IndexOf("<div class=\"col1-3 red miniSpacer\">", paramStartIndex) + 35;
                        }
                        builder.Remove(builder.Length - 1, 1);
                    }
                    string returnType = "void";
                    if (returnStartIndex>-1)
                    {
                        returnStartIndex = body.IndexOf("<div class=\"col1-3 red\">", returnStartIndex) + 24;
                        int returnEndIndex = body.IndexOf("</div>", returnStartIndex);
                        returnType = body.Substring(returnStartIndex, returnEndIndex - returnStartIndex);
                        if (returnType.IndexOf("Array.") > -1)
                        {
                            string[] splitType = returnType.Split('.');
                            returnType = splitType[1] + "[]";
                        }
                    }
                    returnType = returnType.Replace("=", "").Replace("...","");
                    builder.AppendFormat("):{0};\n",returnType);

                }
            return builder.ToString();
        }
    }
}
